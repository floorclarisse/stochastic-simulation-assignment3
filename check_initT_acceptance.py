import numpy as np

from TSP2 import *

def simple_metropolis(MClength, s0, h0, T):
    cost_lst = np.zeros(MClength)   #+1)
    # initial state is a provided circuit with city number 1 the starting and ending city (- on index position 0)
    s1 = s0
    h1 = h0 
    acceptance = 0
    attempts = 0
    n = len(s0) # the length of the circuit

    # for each the 'MClength' iterations sample the next states, and design whether to accept
    for i in range(MClength): #this was MC_length+1, but why???
        # sample the next state
        c1 = np.random.randint(0,n-2) # can get 0, 1, ..., n-3
        c2 = np.random.randint(c1+3,n+1) # can get 3, to n-3+3=n
        s2 = two_opt(s1,c1,c2)  
        h2 = total_distance(s2)

        if s1 == s2:
            print('error')
        # if accept, take the new sample as next state
        if accept(T,h1,h2):     # acceptance decision, a True/False binary variable
            acceptance += 1
            # update for the next iter
            s1 = s2
            h1 = h2
        
        attempts += 1
    
    accept_rate = acceptance / attempts
    return accept_rate

def check_init_acceptance(cities, init_Ts, MClength = 100, samples=100):
    
    for T0 in init_Ts:
        accept_rates = np.zeros(samples)
        for i in range(samples):
            # get random starting permutation
            permutation = np.random.permutation(cities[1:])
            circuit = [cities[0]] + list(permutation)
            # print(total_distance(cities))
            # print(circuit)
            s0 = circuit[:]
            # print(s0)
            h0 = total_distance(s0)
            accept_rates[i] = simple_metropolis(MClength, s0, h0, T0)
    
        print(f'the initial acceptance at init.T {T0} has a mean acceptance rate = {np.mean(accept_rates)}')

fileName = 'a280.tsp'

# init_Ts = np.linspace(5,45,41)
init_Ts = np.linspace(200,300,20)
cities = read_cities(fileName)
compute_distance_pairs(cities)
# check_init_acceptance(cities, init_Ts, MClength = 100, samples=100)
check_init_acceptance(cities, init_Ts, MClength = 500, samples=100)


"""
For a280.tsp, MClength = 500, 100 samples

the initial acceptance at init.T 68.0 has a mean acceptance rate = 0.6265799999999999
the initial acceptance at init.T 74.94736842105263 has a mean acceptance rate = 0.6443800000000001
the initial acceptance at init.T 81.89473684210526 has a mean acceptance rate = 0.66598
the initial acceptance at init.T 88.84210526315789 has a mean acceptance rate = 0.6776000000000001
the initial acceptance at init.T 95.78947368421052 has a mean acceptance rate = 0.6905399999999999
the initial acceptance at init.T 102.73684210526315 has a mean acceptance rate = 0.7025800000000001
the initial acceptance at init.T 109.6842105263158 has a mean acceptance rate = 0.71858
the initial acceptance at init.T 116.63157894736842 has a mean acceptance rate = 0.7346799999999998
the initial acceptance at init.T 123.57894736842105 has a mean acceptance rate = 0.73996
the initial acceptance at init.T 130.5263157894737 has a mean acceptance rate = 0.7525800000000001
the initial acceptance at init.T 137.4736842105263 has a mean acceptance rate = 0.7574199999999999
the initial acceptance at init.T 144.42105263157896 has a mean acceptance rate = 0.7647399999999999
the initial acceptance at init.T 151.3684210526316 has a mean acceptance rate = 0.7749199999999999
the initial acceptance at init.T 158.31578947368422 has a mean acceptance rate = 0.7810400000000002
the initial acceptance at init.T 165.26315789473685 has a mean acceptance rate = 0.79164
the initial acceptance at init.T 172.21052631578948 has a mean acceptance rate = 0.79566
the initial acceptance at init.T 179.1578947368421 has a mean acceptance rate = 0.8027800000000002
the initial acceptance at init.T 186.10526315789474 has a mean acceptance rate = 0.8120199999999999
the initial acceptance at init.T 193.05263157894737 has a mean acceptance rate = 0.8115000000000002
the initial acceptance at init.T 200.0 has a mean acceptance rate = 0.8194399999999998
the initial acceptance at init.T 200.0 has a mean acceptance rate = 0.82098
the initial acceptance at init.T 205.26315789473685 has a mean acceptance rate = 0.8270199999999996
the initial acceptance at init.T 210.5263157894737 has a mean acceptance rate = 0.8286600000000002
the initial acceptance at init.T 215.78947368421052 has a mean acceptance rate = 0.8325599999999999
the initial acceptance at init.T 221.05263157894737 has a mean acceptance rate = 0.8361799999999999
the initial acceptance at init.T 226.31578947368422 has a mean acceptance rate = 0.8400399999999999
the initial acceptance at init.T 231.57894736842104 has a mean acceptance rate = 0.8406200000000001


(lower)

the initial acceptance at init.T 5.0 has a mean acceptance rate = 0.38936
the initial acceptance at init.T 5.9375 has a mean acceptance rate = 0.38604000000000005
the initial acceptance at init.T 6.875 has a mean acceptance rate = 0.39256
the initial acceptance at init.T 7.8125 has a mean acceptance rate = 0.3987
the initial acceptance at init.T 8.75 has a mean acceptance rate = 0.4001599999999999
the initial acceptance at init.T 9.6875 has a mean acceptance rate = 0.41100000000000003
the initial acceptance at init.T 10.625 has a mean acceptance rate = 0.42001999999999995
the initial acceptance at init.T 11.5625 has a mean acceptance rate = 0.42022
the initial acceptance at init.T 12.5 has a mean acceptance rate = 0.42512
the initial acceptance at init.T 13.4375 has a mean acceptance rate = 0.4346400000000001
the initial acceptance at init.T 14.375 has a mean acceptance rate = 0.4396200000000001
the initial acceptance at init.T 15.3125 has a mean acceptance rate = 0.44012
the initial acceptance at init.T 16.25 has a mean acceptance rate = 0.4416400000000001
the initial acceptance at init.T 17.1875 has a mean acceptance rate = 0.44644000000000006
the initial acceptance at init.T 18.125 has a mean acceptance rate = 0.45524000000000003
the initial acceptance at init.T 19.0625 has a mean acceptance rate = 0.4598
the initial acceptance at init.T 20.0 has a mean acceptance rate = 0.46140000000000003
the initial acceptance at init.T 20.9375 has a mean acceptance rate = 0.46875999999999995
the initial acceptance at init.T 21.875 has a mean acceptance rate = 0.46532
the initial acceptance at init.T 22.8125 has a mean acceptance rate = 0.47374000000000005
the initial acceptance at init.T 23.75 has a mean acceptance rate = 0.47702000000000006
the initial acceptance at init.T 24.6875 has a mean acceptance rate = 0.48208000000000006
the initial acceptance at init.T 25.625 has a mean acceptance rate = 0.4924000000000001
the initial acceptance at init.T 26.5625 has a mean acceptance rate = 0.4915
the initial acceptance at init.T 27.5 has a mean acceptance rate = 0.4892600000000001
the initial acceptance at init.T 28.4375 has a mean acceptance rate = 0.50022
the initial acceptance at init.T 29.375 has a mean acceptance rate = 0.5005
the initial acceptance at init.T 30.3125 has a mean acceptance rate = 0.50464
the initial acceptance at init.T 31.25 has a mean acceptance rate = 0.50458
the initial acceptance at init.T 32.1875 has a mean acceptance rate = 0.50898
the initial acceptance at init.T 33.125 has a mean acceptance rate = 0.5126
the initial acceptance at init.T 34.0625 has a mean acceptance rate = 0.5155599999999999
the initial acceptance at init.T 35.0 has a mean acceptance rate = 0.5192800000000001
the initial acceptance at init.T 35.9375 has a mean acceptance rate = 0.5264800000000001
the initial acceptance at init.T 36.875 has a mean acceptance rate = 0.5311199999999999
the initial acceptance at init.T 37.8125 has a mean acceptance rate = 0.5306400000000001
the initial acceptance at init.T 38.75 has a mean acceptance rate = 0.5371
the initial acceptance at init.T 39.6875 has a mean acceptance rate = 0.53912
the initial acceptance at init.T 40.625 has a mean acceptance rate = 0.5435800000000001
the initial acceptance at init.T 41.5625 has a mean acceptance rate = 0.55012
the initial acceptance at init.T 42.5 has a mean acceptance rate = 0.5503999999999999
the initial acceptance at init.T 43.4375 has a mean acceptance rate = 0.55168
the initial acceptance at init.T 44.375 has a mean acceptance rate = 0.55708
the initial acceptance at init.T 45.3125 has a mean acceptance rate = 0.5585
the initial acceptance at init.T 46.25 has a mean acceptance rate = 0.55914
the initial acceptance at init.T 47.1875 has a mean acceptance rate = 0.5649200000000001
the initial acceptance at init.T 48.125 has a mean acceptance rate = 0.5672
the initial acceptance at init.T 49.0625 has a mean acceptance rate = 0.57136
the initial acceptance at init.T 50.0 has a mean acceptance rate = 0.5726600000000001
the initial acceptance at init.T 50.9375 has a mean acceptance rate = 0.57708
the initial acceptance at init.T 51.875 has a mean acceptance rate = 0.5833400000000001
the initial acceptance at init.T 52.8125 has a mean acceptance rate = 0.5864400000000001
the initial acceptance at init.T 53.75 has a mean acceptance rate = 0.5858399999999999
the initial acceptance at init.T 54.6875 has a mean acceptance rate = 0.5917399999999999
the initial acceptance at init.T 55.625 has a mean acceptance rate = 0.59416
the initial acceptance at init.T 56.5625 has a mean acceptance rate = 0.6003200000000001
the initial acceptance at init.T 57.5 has a mean acceptance rate = 0.5956199999999999
the initial acceptance at init.T 58.4375 has a mean acceptance rate = 0.60108
the initial acceptance at init.T 59.375 has a mean acceptance rate = 0.60824
the initial acceptance at init.T 60.3125 has a mean acceptance rate = 0.6078200000000001
the initial acceptance at init.T 61.25 has a mean acceptance rate = 0.6110599999999999
the initial acceptance at init.T 62.1875 has a mean acceptance rate = 0.6158800000000001
the initial acceptance at init.T 63.125 has a mean acceptance rate = 0.61382
the initial acceptance at init.T 64.0625 has a mean acceptance rate = 0.62188
the initial acceptance at init.T 65.0 has a mean acceptance rate = 0.62048
the initial acceptance at init.T 65.9375 has a mean acceptance rate = 0.62252
the initial acceptance at init.T 66.875 has a mean acceptance rate = 0.6259


"""




# for eil51.tsp
# the initial acceptance at init.T 5.0 has a mean acceptance rate = 0.42080000000000006
# the initial acceptance at init.T 6.0 has a mean acceptance rate = 0.4415
# the initial acceptance at init.T 7.0 has a mean acceptance rate = 0.4631
# the initial acceptance at init.T 8.0 has a mean acceptance rate = 0.48
# the initial acceptance at init.T 9.0 has a mean acceptance rate = 0.4987
# the initial acceptance at init.T 10.0 has a mean acceptance rate = 0.5168
# the initial acceptance at init.T 11.0 has a mean acceptance rate = 0.5289
# the initial acceptance at init.T 12.0 has a mean acceptance rate = 0.5513
# the initial acceptance at init.T 13.0 has a mean acceptance rate = 0.5681
# the initial acceptance at init.T 14.0 has a mean acceptance rate = 0.5808
# the initial acceptance at init.T 15.0 has a mean acceptance rate = 0.5807
# the initial acceptance at init.T 16.0 has a mean acceptance rate = 0.6072
# the initial acceptance at init.T 17.0 has a mean acceptance rate = 0.6094999999999999
# the initial acceptance at init.T 18.0 has a mean acceptance rate = 0.6271999999999999
# the initial acceptance at init.T 19.0 has a mean acceptance rate = 0.6397999999999999
# the initial acceptance at init.T 20.0 has a mean acceptance rate = 0.6474
# the initial acceptance at init.T 21.0 has a mean acceptance rate = 0.6641000000000001
# the initial acceptance at init.T 22.0 has a mean acceptance rate = 0.6700000000000002
# the initial acceptance at init.T 23.0 has a mean acceptance rate = 0.677
# the initial acceptance at init.T 24.0 has a mean acceptance rate = 0.6920000000000002
# the initial acceptance at init.T 25.0 has a mean acceptance rate = 0.6971999999999997
# the initial acceptance at init.T 26.0 has a mean acceptance rate = 0.7048000000000001
# the initial acceptance at init.T 27.0 has a mean acceptance rate = 0.7158999999999999
# the initial acceptance at init.T 28.0 has a mean acceptance rate = 0.722
# the initial acceptance at init.T 29.0 has a mean acceptance rate = 0.7319999999999998
# the initial acceptance at init.T 30.0 has a mean acceptance rate = 0.7290000000000002
# the initial acceptance at init.T 31.0 has a mean acceptance rate = 0.7500999999999999
# the initial acceptance at init.T 32.0 has a mean acceptance rate = 0.7506
# the initial acceptance at init.T 33.0 has a mean acceptance rate = 0.7709
# the initial acceptance at init.T 34.0 has a mean acceptance rate = 0.7634000000000001
# the initial acceptance at init.T 35.0 has a mean acceptance rate = 0.7661
# the initial acceptance at init.T 36.0 has a mean acceptance rate = 0.7658999999999999
# the initial acceptance at init.T 37.0 has a mean acceptance rate = 0.7761
# the initial acceptance at init.T 38.0 has a mean acceptance rate = 0.7897
# the initial acceptance at init.T 39.0 has a mean acceptance rate = 0.7808999999999999
# the initial acceptance at init.T 40.0 has a mean acceptance rate = 0.7933999999999999
# the initial acceptance at init.T 41.0 has a mean acceptance rate = 0.7872
# the initial acceptance at init.T 42.0 has a mean acceptance rate = 0.8040999999999998
# the initial acceptance at init.T 43.0 has a mean acceptance rate = 0.8049000000000003
# the initial acceptance at init.T 44.0 has a mean acceptance rate = 0.8009000000000001
# the initial acceptance at init.T 45.0 has a mean acceptance rate = 0.8109999999999999

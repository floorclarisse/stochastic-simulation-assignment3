""" Some code for testing methods"""

import matplotlib.pyplot as plt

def two_opt(circuit,c1,c2):
    """
    Implements 2 opt elementary edit, where two nonadjacent edges are deleted
    and the four cities are reconnected so that a new circuit is created
    In a lin 2-opt algorithm, we pick 2 cities and revert the subtour between them

    Arg:
        circuit: a list of cities by the visiting order
        c1: the location of the first city concerned on the circuit
        c2: the location of the last city concerned on the circuit
    Output:
        circuit: updated circuit with reversed subtour
    """
    if c2 - c1 >= 3:
        # note that when c1 == 0 and c2 == n, the tour is entirely revert, no changes on the cost
        # detect the subtour between the two selected cities
        subtour = circuit[c1+1:c2]
        # reverse the subtour, such that we replace the edge (c1,c1+1), (c2-1,c2)
        # with new edge (c1,c2-1) and (c1+1,c2)
        circuit[c1+1:c2] = subtour[::-1]
        return circuit
    else:
        print('Error! The selected cities cannot be two-opt!')


def read_circuit(fileName):
    with open('TSP-Configurations/' + fileName + '.txt', 'r') as file:
        # read the first line from the file
        line = file.readline()
        # skip the first few lines (that contain info) until indexing of cities starts
        while line.strip()[0] != '1':
            # get next line
            line = file.readline()
        # save the lines of interest
        lines = [line]
        lines = lines + file.readlines()[:-1] # last line is 'EOF'
    
    # translate the text information into a circuit represent by list
    # get the city objects in a list
    circuit = []
    for line in lines:
        # strip and split to get index and x and y coordinates
        city_info = line.strip().strip('\n').split()
        # ix = city_info[0]
        ix = line[0]
        circuit.append(int(float(ix))) 
    
    return circuit

fileName = 'eil51.opt.tour'
#opt_circuit = read_circuit(fileName)
#print(opt_circuit)

# a = [1,2,3,4,5,6]

# a_new = two_opt(a, c1=3, c2=6)
# print(a_new)



##### plot the cooling schedules

def cooling_schedule(temperature,type,cooling_param):
    """
    Implement one of the following 2 cooling_schedule
        Geometrical Cooling Schedule 
        Linear Cooling Schedule  
    Aug:
        temperature: the current temperature
        type: cooling schedule 
        cooling_param: when type is linear, it is the decrement parameter;
                       when type if geometrical, it is the cooling_factor
    Output:
        the updated temperature
    """
    if type == 'linear':
        return temperature - cooling_param
    elif type == 'geometrical':
        return temperature*cooling_param
    else:
        print('Please provide the correct cooling schedule!')


# plot the cooling down schedule for increasing MC number
MClength = 2000
MCnumber = 300000//MClength
MCs = [i for i in range(MCnumber + 1)]
param_geom = 0.95
param_lin = 0.08

T_initial = 172

def iterative_cooling(T_initial, cooling_param, type, MCnumber):
    temps = [T_initial]
    for mc in range(MCnumber):
        if type == 'linear':
            temps.append(temps[mc] - cooling_param)
            # break if linear temperature gets below zero
            if temps[mc+1] < 0:
                temps[mc+1] = 0
        elif type == 'geometrical':
            temps.append(temps[mc] * cooling_param)
    
    return temps

temps_geom = iterative_cooling(T_initial, param_geom, 'geometrical', MCnumber)
temps_lin = iterative_cooling(T_initial, param_lin, 'linear', MCnumber)

# plotting
fig, ax = plt.subplots(1,1,figsize=(8,6))
ax.plot(MCs, temps_lin, color='salmon', label=rf'Linear $T=T-{param_lin}$')
ax.plot(MCs, temps_geom, color='seagreen', label=rf'Geometrical $T=T\cdot{param_geom}$')
ax.set_xlabel('MC number', fontsize=14)
ax.set_ylabel('Temperature', fontsize=14)
ax.set_title(f'Initial temperature {T_initial}')
ax.legend()
fig.tight_layout()
plt.show()
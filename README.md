# Running the code
## Requirements
The following non-standard packages were used in our code:
- matplotlib
- numpy
- tqdm
- scipy

These can be installed by running the following code

`pip install matplotlib`  
`pip install numpy`    
`pip install tqdm`  
`pip install scipy`   

## Running the code
The simulated annealing algorithm is provided in the file TSP2.py

The parameter search has been done in parameter_search.py. Running this file will provide three figures, which will be saved in the Figures directory, using the data stored in the Data directory. The final run of 50 simulations for the final parameter set is also done here.

The algorithm for finding the initial temperature can be found in check_initT_acceptance.py. 

check_opt_distance.py provides a single simulation using the final set of parameters, and also visualizes the optimal tour provided in the assignment.

In testing.py some testing for the 2-opt method and the cooling schedule is done performed, but not of further importance to the research.
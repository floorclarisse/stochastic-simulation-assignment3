import numpy as np
from numpy import random
import matplotlib.pyplot as plt
import math
from tqdm import tqdm


# create Data and Figures directory
import os
if not os.path.exists('Data'):
    os.makedirs('Data')
if not os.path.exists('Figures'):
    os.makedirs('Figures')

"""
For the set up of class object 'city', calculating the total distance of circuit 
and the visualization of circuit, and part of the annealing function,
we adapt the code provided by the following resource

/***************************************************************************************
*    Title: Simulated annealing applied to the traveling salesman problem
*    Author: Emmanuel Goossaert
*    Date:  April 6, 2010
*    Availability: https://codecapsule.com/2010/04/06/simulated-annealing-traveling-salesman/
*
***************************************************************************************/

"""

distances_pair = []


def read_cities(fileName):
    """
    Read city data from a txt file containing index (starting from 1) and x,y coordinates

    Args
    -----
        fileName (string)       name of the textfile, without .txt extension

    Returns
    -------
        cities (list)           list containing City objects
    """
    with open('TSP-Configurations/' + fileName + '.txt', 'r') as file:

        # read the first line from the file
        line = file.readline()
        # skip the first few lines (that contain info) until indexing of cities starts
        while line.strip()[0] != '1':
            # get next line
            line = file.readline()
        
        # save the lines of interest
        lines = [line]
        lines = lines + file.readlines()[:-1] # last line is 'EOF'

    # get the city objects in a list
    cities = []
    for line in lines:
        # strip and split to get index and x and y coordinates
        city_info = line.strip().strip('\n').split()

        ix, x, y = city_info
        
        cities.append(City(int(float(ix)), int(float(x)), int(float(y)))) 
        # note: converting to float first is necessary on the off chance that the 
        # number is written in scientific notation, in which case it will not convert to 
        # integer using int()
       
    return cities


def read_circuit(fileName):
    """
    Function that reads a circuit from a txt file and returns a list of the city objects
    in correct order
    """
    with open('TSP-Configurations/' + fileName + '.txt', 'r') as file:
        # read the first line from the file
        line = file.readline()
        # skip the first few lines (that contain info) until indexing of cities starts
        while line.strip()[0] != '1':
            # get next line
            line = file.readline()
        # save the lines of interest
        lines = [line]
        lines = lines + file.readlines()[:-1] # last line is 'EOF'
    
    # translate the text information into a circuit represent by list
    # get the city objects in a list
    circuit = []
    for line in lines:
        # strip and split to get index and x and y coordinates
        city_info = line.strip().strip('\n').split()
        # ix = city_info[0]
        ix = line[0]
        circuit.append(int(float(ix))) 
    
    return circuit


class City:
    """
    Store information regarding a city, including label and coordinates
    """

    def __init__(self, number=0, xcoord=0, ycoord=0):
        self.number = number
        self.xcoord = xcoord
        self.ycoord = ycoord

    def __str__(self):
       """
       Convert City object to string in case of printing
       """ 
       return 'City number %d; location %d %d' % (self.number, self.xcoord, self.ycoord)

    def __repr__(self):
        """
        Returns the string representation of an object
        Use by calling repr(city)
        
        """
        return self.__str__()

    def compute_distance_to_city(self, city):
        """
        Euclidean distance to another city
        """
        return np.sqrt((self.xcoord - city.xcoord)**2 + (self.ycoord - city.ycoord)**2)
    
    def distance_to_city(self, city):
        """
        Getting (euclidean) distance to another city from pre-computed list
        """
        global distances_pair

        # check if other city is not equal to current city
        if self.number != city.number:
            # get indices of current and other city
            numbers = [self.number, city.number]
            # get pre-computed distance from ditances_pair list
            # where indices are ordered biggest, lowest
            return distances_pair[max(numbers)-1][min(numbers)-1]
            
        return 0

def compute_distance_pairs(cities):
    """
    Compute all distance pairs
    Important: The cities must be ordered by their numbers in the list.
    """
    global distances_pair

    # go through all cities
    for city_from in cities:
        # create list of city distance of correct length
        distances_pair.append([0 for _ in range(city_from.number)]) 

        # go through all cities with index smaller than city_from
        for city_to in cities[:city_from.number]:
            # compute distance and save in the list
            distances_pair[city_from.number-1][city_to.number-1] = city_from.compute_distance_to_city(city_to)

def total_distance(cities):
    """
    Computes total distances of a circuit through all cities, which are in order given by cities

    Args:
        cities (list):  contains city objects in order of circuit
    Returns:
        float           total distance of circuit
    """
    
    distances = [cities[index].distance_to_city(cities[ (index + 1) % len(cities) ]) for index in range(len(cities))]
    return sum(distances)

def plot_cities(cities, ax_map):
    """
    Plot the cities on a plane
    """
    cities_x = [city.xcoord for city in cities + [cities[0]]]
    cities_y = [city.ycoord for city in cities + [cities[0]]]

    link = '-'
    ax_map.plot(cities_x, cities_y, 'go' + link)
    ax_map.grid()

    spacing = math.fabs(min(cities_x) - max(cities_x)) * .1
    ax_map.set_xlim(min(cities_x) - spacing, max(cities_x) + spacing)
    ax_map.set_ylim(min(cities_y) - spacing, max(cities_y) + spacing)

    return ax_map

def two_opt(circuit,c1,c2):
    """
    Implements 2 opt elementary edit, where two nonadjacent edges are deleted
    and the four cities are reconnected so that a new circuit is created
    In a lin 2-opt algorithm, we pick 2 cities and revert the subtour between them

    Arg:
        circuit: a list of cities by the visiting order
        c1: the location of the first city concerned on the circuit
        c2: the location of the last city concerned on the circuit
    Output:
        circuit: updated circuit with reversed subtour
    """
    new_circuit = circuit.copy()
    if c2 - c1 >= 3:
        # note that when c1 == 0 and c2 == n, the tour is entirely revert, no changes on the cost
        # detect the subtour between the two selected cities
        subtour = new_circuit[c1+1:c2]
        # reverse the subtour, such that we replace the edge (c1,c1+1), (c2-1,c2)
        # with new edge (c1,c2-1) and (c1+1,c2)
        new_circuit[c1+1:c2] = subtour[::-1]
        return new_circuit
    else:
        print('Error! The selected cities cannot be two-opt!')
        # return original circuit
        return circuit    

def accept(T,h1,h2):
    """
    The function gives the accept / reject decision given the current and next circuits 
    and current temperature T
    Arg:
        T: temperature 
        h1: cost of current circuit
        h2: cost of the sampled candidate circuit
    Output:
        True: accept
        False: reject
    """

    # calculate the acceptance parameter A
    # A = min([1,np.exp(-(h2-h1)/T)])
    # to avoid overflow error
    if h2 < h1:
        # A = 1
        # cost reduced at s2, then always accept
        return True
    else:
        A = np.exp(-(h2-h1)/T)
        random_pointer = np.random.random()
        if random_pointer <= A:
            # accept by chance
            return True
        else:
            # reject by chance
            return False

def metropolis(MClength, s0, h0, T, h_best, s_best):
    """
    At each limited length Markov chain, we start from a random state
    and then we apply 2-opt algorithm to sample the next state.
    The acceptance of next state depends on the accept rate function A_ij

    Arg:
        MClength: the length of the current Markov chain
        s0: the initial state / circuit
        h0: the cost of the initial state
        T: the current temperature
        h_best: the smallest distance found so far (for stop criterion)
        s_best: the current best solution's
    
    Output:
        s_final: the last state / circuit of this Markov chain
        best_sol: the current best solution's
        best_cost: the current best solution's total distance 
        cost_lst: the list of cost function value for each state of this Markov chain

    """
    cost_lst = np.zeros(MClength)
    # initial state is a provided circuit with city number 1 the starting and ending city (- on index position 0)
    s1 = s0
    h1 = h0 

    n = len(s0) # the length of the circuit
    best_sol = s_best
    best_cost = h_best

    # for each the 'MClength' iterations sample the next states, and design whether to accept
    for i in range(MClength):
        # sample the next state
        c1 = np.random.randint(0,n-2) # can get 0, 1, ..., n-3
        c2 = np.random.randint(c1+3,n+1) # can get 3, to n-3+3=n

        s2 = two_opt(s1,c1,c2)
        
        h2 = total_distance(s2)

        if s1 == s2:
            print('error')
        # if accept, take the new sample as next state
        if accept(T,h1,h2):     # acceptance decision, a True/False binary variable
            # record the cost of the new state
            cost_lst[i] = h2
            # update the current best
            if h2 < best_cost:
                best_cost = h2
                best_sol = s2
            # update for the next iter
            s1 = s2
            h1 = h2
        else:
        # if reject, take the current state as the next state
            cost_lst[i] = h1
        
    s_final = s1
    
    return s_final, best_sol, best_cost, cost_lst

def cooling_schedule(temperature,type,cooling_param):
    """
    Implement one of the following 2 cooling_schedule
        Geometrical Cooling Schedule 
        Linear Cooling Schedule  
    Aug:
        temperature: the current temperature
        type: cooling schedule 
        cooling_param: when type is linear, it is the decrement parameter;
                       when type if geometrical, it is the cooling_factor
    Output:
        the updated temperature
    """
    if type == 'linear':
        return temperature - cooling_param
    elif type == 'geometrical':
        return temperature*cooling_param
    else:
        print('Please provide the correct cooling schedule!')


def annealing(cities, temperature_begin=20.1, temperature_end=.1, cooling_factor=.975, decrement_param = 0.1, 
              nb_iter=25000, MClength = 50, cooling_type='geometrical', save_all=False):
    """
    Simulated annealing function, implemented with acceptance probability by Kirkpatrick et al., and with restart
    
    distance_best:      best solution encountered so far
    distance_current:   solution used in the current simulation
    distance_new:       solution compted from the random changes to current
    """
    # the number of MC we will create is a function of a constant calculation budget nb_iter
    MC_amount = nb_iter // MClength

    # get random starting permutation
    permutation = np.random.permutation(cities[1:])
    circuit = [cities[0]] + list(permutation)

    circuit_current = circuit[:]
    distance_current = total_distance(circuit_current)

    # to save the best of each markov chain of length MClength
    distances_best = np.zeros(MC_amount + 1)
    distances_last = np.zeros(MC_amount + 1)
    distances_avg = np.zeros(MC_amount)

    # only save all distances when we want to
    distances_all = None
    if save_all:
        distances_all = np.zeros(nb_iter+1)
        distances_all[0] = distance_current

    # save distance of random starting permutation
    distances_best[0] = distance_current
    distances_last[0] = distance_current

    # current best solution is original circuit
    best_sol = circuit_current[:]
    best_cost = distance_current

    temperature = temperature_begin
    for mc in range(MC_amount):


        # if we implement stop criterion 
        step = 0

        """
            apply Metropolis
            never touch the first city (it does not need to change)
            we've chosen the first city is the one with number 1

        """
        circuit_current, best_sol, best_cost, cost_list = metropolis(MClength=MClength, s0=circuit_current, h0=distance_current,
                                                                T=temperature, h_best=best_cost, s_best=best_sol)
        
        if save_all:
            distances_all[1+mc*MClength:1+(mc+1)*MClength] = cost_list


        # continue from last solutionof the metropolis, called ci
        distance_current = total_distance(circuit_current)
        # update temperature and step
        if cooling_type == 'linear':
            temperature = cooling_schedule(temperature=temperature,type=cooling_type,cooling_param=decrement_param)
        elif cooling_type == 'geometrical':
            temperature = cooling_schedule(temperature=temperature,type=cooling_type,cooling_param=cooling_factor)
        
        distances_best[mc+1] = best_cost
        distances_last[mc+1] = distance_current
        distances_avg[mc] = np.mean(cost_list) # note the initial cost is not taken into account here!!
        step += 1

        if temperature <= 0: 
            return circuit_current, distance_current, distances_best, best_sol, distances_last, mc, distances_all, distances_avg


    return circuit_current, distance_current, distances_best, best_sol, distances_last, mc, distances_all, distances_avg



def single_simulation(fileName_city, temperature_begin=40.1, temperature_end=.01, cooling_factor=.975, decrement_param = 0.1, 
                        nb_iter=25000, MClength = 50, cooling_type='geometrical', save=False, show=True):
    """
    Running and visualizing circuit distance for a single simulation

    """
    # read cities and its optimal solution from txt file
    cities = read_cities(fileName)

    # compute and save distances between cities to be used later
    compute_distance_pairs(cities)

    np.random.seed(924724)
    ## this is for plotting, because starting circuit is made randomly in annealing function now
    # get random starting permutation
    permutation = np.random.permutation(cities[1:])
    circuit_begin = [cities[0]] + list(permutation)
    distance_begin = total_distance(circuit_begin)


    # run the simulation once
    np.random.seed(924724)

    circuit_final, distance_final, distances_best, best_sol, distances_last, mc, distances_all, _ = annealing(cities, temperature_begin, temperature_end, cooling_factor, decrement_param, 
                                                                            nb_iter, MClength, cooling_type, save_all=True)
    
    fig, axs = plt.subplots(2,3,figsize=(15,10))

    axs[0,0] = plot_cities(circuit_begin, axs[0,0])
    axs[0,1] = plot_cities(circuit_final, axs[0,1])
    axs[0,2] = plot_cities(best_sol, axs[0,2])
    axs[0,0].set_title('Initial (random) tour on %d cities.  Distance: %.0f km' % (len(circuit_begin), distance_begin), fontsize=14)
    axs[0,1].set_title('Final tour on %d cities.  Distance: %.0f km' % (len(circuit_begin), distance_final), fontsize=14)
    axs[0,2].set_title('Best tour on %d cities.  Distance: %.0f km' % (len(circuit_begin), total_distance(best_sol)), fontsize=14)

    axs[0,0].set_ylabel('y-coordinate', fontsize=12)
    for i in range(3):
        axs[0,i].set_xlabel('x-coordinate', fontsize=12)


    axs[1,0].set_title('Progression of distance', fontsize=14)
    axs[1,0].plot([i for i in range(nb_iter+1)], distances_all, color='magenta')
    axs[1,0].set_xlabel('Iteration', fontsize=12)
    axs[1,0].set_ylabel('Distance (km)', fontsize=12)

    axs[1,1].set_title('Last solution of each MC', fontsize=14)
    axs[1,1].plot([i for i in range(nb_iter//MClength + 1)], distances_last, color='deepskyblue')
    axs[1,1].set_xlabel('Markov Chain Number')

    axs[1,2].set_title('Best solution of each MC', fontsize=14)
    axs[1,2].plot([i for i in range(nb_iter//MClength + 1)], distances_best, color='salmon')
    axs[1,2].set_xlabel('Markov Chain Number')

    problem = fileName.split('.')
    if cooling_type == 'geometrical':
        fig.suptitle(rf'{nb_iter//MClength} Markov Chains of length {MClength} for the {problem[0]} TSP; $T_0$={temperature_begin} '+
                     rf'{cooling_type} cooling, with parameter {cooling_factor}', fontsize=16)
    elif cooling_type == 'linear':
        fig.suptitle(rf'{nb_iter//MClength} Markov Chains of length {MClength} for the {problem[0]} TSP; $T_0$={temperature_begin} '+
                     rf'{cooling_type} cooling, with parameter {decrement_param}', fontsize=16)
    
    fig.tight_layout()
    
    if save:
        name = 'Figures/singlesimulation_nbiter' + str(nb_iter) + '_MC' + str(MClength) + '_T0' + str(temperature_begin) + str(cooling_type)
        plt.savefig(name + '.svg')
    if show:
        plt.show()

def multiple_simulation(cities, temperature_begin=40.1, temperature_end=.01, cooling_factor=.975, decrement_param = 0.1, 
                        nb_iter=25000, MClength = 50, cooling_type='geometrical', Nsim=2, plot=False, saveplot=False, show=False,
                        savedata=False, saveName='test'):
    """
    Running multiple simulations for the same parameter set and obtaining statistics
    Saves the cost of best and last solution of each MC
    Saves averages of each MC

    Option to plot and save figure is available

    """

    # things to save
    allsim_final_distances = np.zeros((Nsim, nb_iter//MClength))
    allsim_best_distances = np.zeros((Nsim, nb_iter//MClength))
    initial_distances = np.zeros(Nsim)
    
    # average distance for each MC
    avg_distance_MCs = np.zeros((Nsim, nb_iter//MClength))
    
    # run the simulation multiple times
    for sim in range(Nsim):
        circuit_final, distance_final, distances_best, best_sol, distances_last, mc, distances_all, distances_avg = annealing(cities, temperature_begin, temperature_end, cooling_factor, decrement_param, 
                                                                                                                              nb_iter, MClength, cooling_type, save_all=False)
    
        # save data
        initial_distances[sim] = distances_best[0]
        allsim_final_distances[sim,:] = distances_last[1:] # because the first entry is distance of starting permutation
        allsim_best_distances[sim,:] = distances_best[1:]
        avg_distance_MCs[sim,:] = distances_avg

    # get mean and std over the multiple simulations
    initial_distances_mean = np.mean(initial_distances)
    initial_distances_std = np.std(initial_distances)
    
    final_distances_mean = np.mean(allsim_final_distances, axis=0)
    final_distances_std = np.std(allsim_final_distances, axis=0)

    best_distances_mean = np.mean(allsim_best_distances, axis=0)
    best_distances_std = np.std(allsim_best_distances, axis=0)

    avg_distances_mean = np.mean(avg_distance_MCs, axis=0)
    avg_distances_std = np.std(avg_distance_MCs, axis=0)    

    MCs = [i for i in range(nb_iter//MClength + 1)]

    if plot:
        # plotting
        fig, axs = plt.subplots(1,3, figsize=(16,8))
        axs[0].set_title('Last solution of each MC', fontsize=12)
        axs[0].plot(0, initial_distances_mean, label='initial circuit')
        axs[0].plot(MCs[1:], final_distances_mean, label='last circuit') 
        axs[0].fill_between(MCs[1:], final_distances_mean - final_distances_std, final_distances_mean + final_distances_std, alpha=0.2)

        axs[1].set_title('Progression of best solution', fontsize=12)
        axs[1].plot(MCs[1:], best_distances_mean, label='best circuit')
        axs[1].fill_between(MCs[1:], best_distances_mean - best_distances_std, best_distances_mean + best_distances_std, alpha=0.2)

        axs[2].set_title('Average cost of each MC', fontsize=12)
        axs[2].plot(MCs[1:], avg_distances_mean, label='average over MC')
        axs[2].fill_between(MCs[1:], avg_distances_mean - avg_distances_std, avg_distances_mean + avg_distances_std, alpha=0.2)

        fig.tight_layout()


        if saveplot:
            plt.savefig('Figures/' + saveName + '.svg')
        if show:
            plt.show()

    if savedata:
        if not os.path.exists('Data/' + saveName):
            os.makedirs('Data/' + saveName)
        np.save('Data/' + saveName + '/initial_distances', initial_distances)
        np.save('Data/' + saveName + '/final_distances', allsim_final_distances)
        np.save('Data/' + saveName + '/best_distances', allsim_best_distances)
        np.save('Data/' + saveName + '/avg_distances', avg_distance_MCs)
    
    
    data = np.zeros((6, nb_iter // MClength))
    
    data[0,:] = final_distances_mean
    data[1,:] = final_distances_std

    data[2,:] = best_distances_mean
    data[3,:] = best_distances_std

    data[4,:] = avg_distances_mean
    data[5,:] = avg_distances_std
    
    return initial_distances_mean, initial_distances_std, data


if __name__ == '__main__':

    # read cities from txt file
    fileName = 'eil51.tsp'
    cities = read_cities(fileName)


    compute_distance_pairs(cities)
    distance_begin = total_distance(cities)


    # single_simulation(fileName, temperature_begin=20.1, temperature_end=.01, cooling_factor=.975, decrement_param = 0.1, 
    #                     nb_iter=25000, MClength = 500, cooling_type='linear')

    np.random.seed(28797)
    multiple_simulation(fileName, temperature_begin=40.1, temperature_end=.01, cooling_factor=.975, decrement_param = 0.1, 
                        nb_iter=25000, MClength = 50, cooling_type='geometrical', Nsim=20, plot=True, saveplot=False, show=True,
                        savedata=True, saveName='test')


    # ax_map = plot_cities(cities, 1)
    # ax_map.set_title('Initial tour on %d cities\nDistance: %.0f km (visiting order is index order)' % (len(cities), distance_begin))

    # # create save fig functions and comment out plt.show() later
    # plt.show()

    # before real simulation, acceptance rate at init.T!
    # this is done by setting nb_mc = 1



    # s_final, best_sol, best_cost, cost_lst = metropolis(MClength=500, s0=cities, h0=distance_begin, T=1, h_best=distance_begin, s_best=cities)


    # print('s_final')
    # for city in s_final:
    #     print(repr(city))

    # print('best_sol')
    # for city in best_sol:
    #     print(repr(city))

    # print('best_cost', best_cost)
    # print('cost list', cost_lst)

    # if s_final == best_sol:
    #     print('final=best')




    # cities_current, distance_current, distances_best, best_sol, distances_last, mc = annealing(cities, temperature_begin=20.1, temperature_end=.01, cooling_factor=.975, decrement_param = 0.1, 
    #                                                                         nb_iter=25000, MClength = 500, cooling_type='geometrical')
    # print(cities_current)
    # print(distance_current)
    # print(distances_best)
    # print(best_sol)
    # print(distances_last)
    # print(mc)
    # if cities_current == best_sol:
    #     print('last=best')
                                                            
""" Parameter search on the TSP
Code for simulations is commented out
Running this file as is will provide the figures used in the report
"""


from TSP2 import *

from scipy.stats import norm

np.random.seed(280797)

# load the TSP
fileName = 'a280.tsp'
cities = read_cities(fileName)
compute_distance_pairs(cities)

# parameters for the geometrical process
# we want a high initial acceptance rate

"""For a280.tsp
the initial acceptance at init.T 172.21052631578948 has a mean acceptance rate = 0.79566
the initial acceptance at init.T 74.94736842105263 has a mean acceptance rate = 0.6443800000000001
the initial acceptance at init.T 102.73684210526315 has a mean acceptance rate = 0.7025800000000001
"""
# T_begin = [172, 75, 103] # intial choice
T_begin = [172]

# cooling factor is typically between 0.8 and 0.99 according to https://web.archive.org/web/20190430051007id_/https://hal-enac.archives-ouvertes.fr/hal-01887543/document
# cooling_factor = np.linspace(0.8, 0.99, 10) #--> initial choices

cooling_factor = [0.95] # --> new choices

# length of the chain should be long enough to reach (quasi-)equilibrium
#MClength = np.linspace(50, 500, 6, dtype=int)
MClength = [2000]

plot = True
show = True
savePlot = True
Nsim = 50
nb_iter = 300000
saveName = fileName + 'geometrical_cf_T_nbiter' + str(len(cooling_factor)) + str(T_begin) + str(nb_iter) + str(Nsim)
print(saveName)


def get_cmap(n, name='hsv'):
    '''
    code from https://stackoverflow.com/questions/14720331/how-to-generate-random-colors-in-matplotlib
    Returns a function that maps each index in 0, 1, ..., n-1 to a distinct 
    RGB color; the keyword argument name must be a standard mpl colormap name.
    '''
    return plt.cm.get_cmap(name, n)



line_style = ['-', '-.', '--']
colors = get_cmap(len(cooling_factor)+1)

k = 0

"""
NOTE the following commented section provides code for running parameter tests. Please keep commented to avoid overwriting existing data
The code below will provide plots of the data
"""

# running the simulations

# fig, axs = plt.subplots(2,len(MClength)+1, figsize=(6*len(MClength),10))
# for m, MCl in tqdm(enumerate(MClength)):
#     data_per_mc = np.zeros((len(T_begin), len(cooling_factor), 2, nb_iter//MCl + 1))


#     for t, T in tqdm(enumerate(T_begin)):  
#         k = 0 
#         for cf in tqdm(cooling_factor):            
#             initial_mean, initial_std, data = multiple_simulation(cities, temperature_begin=T, temperature_end=.001, cooling_factor=cf, 
#                                                       nb_iter=nb_iter, MClength = MCl, cooling_type='geometrical', Nsim=Nsim)
        
            
            
#             final_distances_mean = data[0,:]
#             final_distances_std = data[1,:]
#             best_distances_mean = data[2,:]
#             avg_distances_mean = data[4,:]

#             stacked_means = np.insert(final_distances_mean, 0, initial_mean)
#             stacked_std = np.insert(final_distances_std, 0, initial_std)

#             data_per_mc[t,k,0,:] = stacked_means
#             data_per_mc[t,k,1,:] = stacked_std
            
#             #np.save('Data/ParameterSearch/bestdistancestd' + saveName + 'MClength' + str(MCl), best_means)
            
            
#             color = colors(k)
#             k+=1

#             MCs = [i for i in range(nb_iter//MCl + 1)]

#             if plot:
#                 # plotting
#                 label = rf'(T, $\alpha$) = ({T}, {cf})'
#                 axs[0,m].set_title(f'Last sol; MClength = {MCl}', fontsize=12)
#                 #axs[0,m].plot(0, initial_mean, ls=line_style[0], color=color, label=label)
#                 axs[0,m].plot(MCs, stacked_means, ls=line_style[t], color=color, label=label) 
#                 #axs[0].fill_between(0, initial_mean - initial_std, initial_mean + initial_std, alpha=0.2)
#                 #axs[0].fill_between(MCs[1:], final_distances_mean - final_distances_std, final_distances_mean + final_distances_std, alpha=0.2)
#                 axs[0,m].set_xlabel('MC number', fontsize=12)
#                 axs[1,m].set_title(f'Best sol; MClength = {MCl}', fontsize=12)
#                 axs[1,m].plot(MCs[1:], best_distances_mean, ls=line_style[t], color=color)
#                 #axs[1].fill_between(MCs[1:], best_distances_mean - best_distances_std, best_distances_mean + best_distances_std, alpha=0.2)

#                 #axs[2].set_title('Average cost of each MC', fontsize=12)
#                 #axs[2].plot(MCs[1:], avg_distances_mean, ls=line_style[t], color=color)
#                 #axs[2].fill_between(MCs[1:], avg_distances_mean - avg_distances_std, avg_distances_mean + avg_distances_std, alpha=0.2)

#     if not os.path.exists('Data/ParameterSearch1'):
#         os.makedirs('Data/ParameterSearch1')
            
#     np.save('Data/ParameterSearch1/' + saveName + 'MClength' + str(MCl), data_per_mc)

# axs[0,0].legend(loc='upper right')
# axs[0,0].set_ylabel('Distance', fontsize=16)   

# problem = fileName.split('.')
# fig.suptitle(f'Various cooling parameters for geometrical cooling; {problem[0]} TSP, total number of iterations {nb_iter}', fontsize=14)


# fig.tight_layout()


# if savePlot:
#     plt.savefig('Figures/' + saveName + '.svg')


# if show:
#     plt.show()


"""
FIRST PARAMSWEEP
"""


nb_iter=30000

T_begin = [172, 75, 103]
cooling_factor = np.linspace(0.8, 0.99, 10)

MClength = [50, 230, 410, 1000, 2000]
saveName = fileName + 'geometrical_cf_T_' + str(len(cooling_factor)) + str(T_begin)
fig, axs = plt.subplots(len(MClength),3,figsize=(10,16.5))
colors = get_cmap(len(cooling_factor)+1)

for m, MCl in enumerate(MClength):
    MCs = [i for i in range(nb_iter//MCl + 1)]
    
    # load the saved data for this MC length chain
    data = np.load('Data/ParameterSearch1/' + saveName + 'MClength' + str(MCl) + '.npy')
    # shape of data is T_begin, cooling_factor, mean + std final, MCnr + 1

    for t, T in enumerate(T_begin):
        ts = [2,0,1] # to reorder
        tt = ts[t]
        for k, cf in enumerate(cooling_factor):  
            color = colors(k)
            
            stacked_means = data[t,k,0,:]
            label = rf'$\alpha$ = %.2f' % cf

  
            axs[m,tt].plot(MCs[:], stacked_means, ls = '-', color=color, label=label, linewidth=1)
            axs[m,tt].set_title(rf'MC length {MCl}; $T_0$ {T}', fontsize=10)
        
        axs[m,tt].grid()

        axs[len(MClength)-1,tt].set_xlabel('MC number', fontsize=10)

    axs[m,0].set_ylabel('Distance', fontsize=10)

handles, labels = axs[0,0].get_legend_handles_labels()
#test = axs[0,0].get_legend()

axs[0,0].legend(handles[:4], labels[:4])
axs[0,1].legend(handles[4:7], labels[4:7])
axs[0,2].legend(handles[7:], labels[7:])

problem = fileName.split('.')
fig.suptitle(rf'Parameter sweep for the {problem[0]} TSP, geometrical cooling, total HM iterations {nb_iter}, average over 20 simulations')

fig.tight_layout()
plt.savefig('Figures/Parametersearch2.svg')
plt.show()

"""
SECOND PARAMSWEEP
"""

Nsim=20
nb_iter = 100000
T_begin = [172]
t = 0
T = T_begin[t]
cooling_factor = [0.91, 0.93, 0.95]
MClength = [1000, 2000]
colors = get_cmap(len(cooling_factor)+1)
saveName = fileName + 'geometrical_cf_T_nbiter' + str(len(cooling_factor)) + str(T_begin) + str(nb_iter)

fig, axs = plt.subplots(1,len(MClength),figsize=(10,5))

for m, MCl in enumerate(MClength):
    MCs = [i for i in range(nb_iter//MCl + 1)]
    
    # load the saved data for this MC length chain
    data = np.load('Data/ParameterSearch1/' + saveName + 'MClength' + str(MCl) + '.npy')
    # shape of data is T_begin, cooling_factor, mean + std final, MCnr + 1
    
    for k, cf in enumerate(cooling_factor):  
        color = colors(k)
        
        stacked_means = data[t,k,0,:]
        stacked_stds = data[t,k,1,:]
        label = rf'$\alpha$ = %.2f' % cf

        axs[m].plot(MCs[:], stacked_means, ls = '-', color=color, label=label, linewidth=1)
        axs[m].fill_between(MCs, stacked_means - stacked_stds, stacked_means + stacked_stds, color=color, alpha=0.2)
        axs[m].set_title(rf'MC length {MCl}; $T_0$ {T}', fontsize=12)

        p=0.95
        CV = norm.ppf(p+(1-p)/2)
        # rescale std to get the sample standard deviation with correct degrees of freedom (1/n-1)
        sample_stdev = (stacked_stds[-1]*Nsim)/(Nsim-1)
        #sample_stdev = stacked_stds[-1]
        bound_CI = (CV * sample_stdev) / np.sqrt(Nsim)
        print('Parameterset (T,a) = (%d,%.2f)' % (MCl, cf))
        print('The 95percent-confidence interval is given by: [%.4f, %.4f]' % (stacked_means[-1] - bound_CI, stacked_means[-1] + bound_CI))

        percentage_from_mean = (bound_CI / stacked_means[-1]) * 100
        print('This CI is the avg plus minus %.2f percent of the mean' % percentage_from_mean)
        print('The mean is', stacked_means[-1])
        print('The std is ', stacked_stds[-1])




    axs[m].set_xlabel('MC number', fontsize=12)

    axs[0].set_ylabel('Distance', fontsize=12)

    axs[m].grid()
    axs[m].tick_params(which='both', labelsize=12)

axs[0].legend(fontsize=13)



problem = fileName.split('.')
fig.suptitle(rf'Parameter sweep for {problem[0]} TSP; geometrical cooling; total HM iterations {nb_iter}; 20 simulations', fontsize=13)

fig.tight_layout()
plt.savefig('Figures/Parametersearch3.svg')
plt.show()


"""
FINAL RUNS
"""


Nsim = 50
nb_iter = 300000
T_begin = [172]
t = 0
T = T_begin[t]
cooling_factor = [0.95]
MClength = [2000]
colors = get_cmap(len(cooling_factor)+1)
saveName = fileName + 'geometrical_cf_T_nbiter' + str(len(cooling_factor)) + str(T_begin) + str(nb_iter) + str(Nsim)

fig, axs = plt.subplots(1,1,figsize=(8,5))

for m, MCl in enumerate(MClength):
    MCs = [i for i in range(nb_iter//MCl + 1)]
    
    # load the saved data for this MC length chain
    data = np.load('Data/ParameterSearch1/' + saveName + 'MClength' + str(MCl) + '.npy')
    # shape of data is T_begin, cooling_factor, mean + std final, MCnr + 1
    
    for k, cf in enumerate(cooling_factor):  
        color = colors(k)
        
        stacked_means = data[t,k,0,:]
        stacked_stds = data[t,k,1,:]
        label = rf'$\alpha$ = %.2f' % cf

        axs.plot(MCs[:], stacked_means, ls = '-', color=color, label=label, linewidth=1)
        axs.fill_between(MCs, stacked_means - stacked_stds, stacked_means + stacked_stds, color=color, alpha=0.2)
        axs.set_title(rf'MC length {MCl}; $T_0$ {T}', fontsize=12)

    axs.set_xlabel('MC number', fontsize=12)

    axs.set_ylabel('Distance', fontsize=12)
    axs.tick_params(which='both', labelsize=12)

    axs.grid()

axs.legend(fontsize=13)


problem = fileName.split('.')
fig.suptitle(rf'{problem[0]} TSP; geometrical cooling; total HM iterations {nb_iter}; {Nsim} simulations', fontsize=13)

fig.tight_layout()
plt.savefig('Figures/Parametersearch5.svg')
plt.show()



p=0.95
CV = norm.ppf(p+(1-p)/2)
# rescale std to get the sample standard deviation with correct degrees of freedom (1/n-1)
sample_stdev = (stacked_stds[-1]*Nsim)/(Nsim-1)
#sample_stdev = stacked_stds[-1]
bound_CI = (CV * sample_stdev) / np.sqrt(Nsim)

print('The 95percent-confidence interval is given by: [%.4f, %.4f]' % (stacked_means[-1] - bound_CI, stacked_means[-1] + bound_CI))

percentage_from_mean = (bound_CI / stacked_means[-1]) * 100
print('This CI is the avg plus minus %.2f percent of the mean' % percentage_from_mean)
print('The mean is', stacked_means[-1])

# The 95percent-confidence interval is given by: [2919.0903, 2951.1086]
# This CI is the avg plus minus 0.55 percent of the mean
# The mean is 2935.0994243223768

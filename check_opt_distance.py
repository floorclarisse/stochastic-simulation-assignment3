import numpy as np

from TSP2 import *

def read_order(fileName):
    with open('TSP-Configurations/' + fileName + '.txt', 'r') as file:
        # read the first line from the file
        line = file.readline()
        # skip the first few lines (that contain info) until indexing of cities starts
        while line.strip()[0] != '1':
            # get next line
            line = file.readline()
        # save the lines of interest
        lines = [line]
        lines = lines + file.readlines()[:-1] # last line is 'EOF'
    
    # translate the text information into a circuit represent by list
    # get the city objects in a list
    circuit = []
    for line in lines:
        # strip and split to get index and x and y coordinates
        city_info = line.strip().strip('\n').split()
        # ix = city_info[0]
        ix = line
        circuit.append(int(float(ix))) 
    
    return circuit

# # read cities from txt file
# # fileName_circuit = 'eil51.opt.tour'
# # fileName_city = 'eil51.tsp'

# # fileName_circuit = 'pcb442.opt.tour'
# # fileName_city = 'pcb442.tsp'

# cities = read_cities(fileName_city)
# print('***** print the cities *****')
# # print(cities)

# opt_order = read_order(fileName_circuit)

# print('***** print whether the opt_order list contains number type data*****')
# print(all(isinstance(n, int) for n in opt_order))
# print('***** print the opt_order list *****')
# print(opt_order)
# print('***** print the length of opt_order list *****')
# print(len(opt_order))

# opt_circuit = []
# for idx in opt_order:
#     opt_circuit.append(cities[idx-1])

# print('***** print optimally ordered cities object list *****')
# # print(opt_circuit)

# compute_distance_pairs(cities)
# distance_opt= total_distance(opt_circuit)
# print('***** print optimal distance *****')
# print(distance_opt)

# fig, axs = plt.subplots(2,3,figsize=(15,10))
# plot_cities(opt_circuit, axs[0,0])
# plt.show()

def compare_solutions(fileName_city, fileName_circuit, temperature_begin=40.1, temperature_end=.01, cooling_factor=.975, decrement_param = 0.1, 
                        nb_iter=25000, MClength = 50, cooling_type='geometrical', save=False, show=True):
    """
    Running and visualizing circuit distance for a single simulation

    """
    # read cities and its optimal solution from txt file
    cities = read_cities(fileName_city)

    # read the information about given optimal tour
    opt_order = read_order(fileName_circuit)
    opt_circuit = []
    for idx in opt_order:
        opt_circuit.append(cities[idx-1])
    # print(len(opt_circuit))
    # print(opt_order)

    # compute and save distances between cities to be used later
    compute_distance_pairs(cities)
    distance_opt= total_distance(opt_circuit)
    
    np.random.seed(92473)
    ## this is for plotting, because starting circuit is made randomly in annealing function now
    # get random starting permutation
    permutation = np.random.permutation(cities[1:])
    circuit_begin = [cities[0]] + list(permutation)
    distance_begin = total_distance(circuit_begin)



    # run the simulation once
    np.random.seed(92473)

    # TODO make sure the best_sol would be the best solution over ALL markov chains, not just last
    circuit_final, distance_final, distances_best, best_sol, distances_last, mc, distances_all, _ = annealing(cities, temperature_begin, temperature_end, cooling_factor, decrement_param, 
                                                                            nb_iter, MClength, cooling_type, save_all=True)
    
    #fig_map = plt.figure(figure_id)
    #ax_map = fig_map.add_subplot(111)

    fig, axs = plt.subplots(2,3,figsize=(15,10))
    for i in range(2):
        for j in range(3):
            axs[i,j].tick_params(axis='both', labelsize=12)
            # axs[i,j].ticklabel_format(useOffset=True)

    axs[1,0].ticklabel_format(axis="both", style="sci", scilimits=(0,0))
    axs[1,1].ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    axs[1,2].ticklabel_format(axis="y", style="sci", scilimits=(0,0))



    axs[0,0] = plot_cities(opt_circuit, axs[0,0])
    axs[0,1] = plot_cities(circuit_final, axs[0,1])
    axs[0,2] = plot_cities(best_sol, axs[0,2])
    axs[0,0].set_title('Provided optimal tour on %d cities.  Distance: %.0f km' % (len(opt_circuit), distance_opt), fontsize=13)
    axs[0,1].set_title('Final tour on %d cities.  Distance: %.0f km' % (len(circuit_begin), distance_final), fontsize=13)
    axs[0,2].set_title('Best tour on %d cities.  Distance: %.0f km' % (len(circuit_begin), total_distance(best_sol)), fontsize=13)

    axs[0,0].set_ylabel('y-coordinate', fontsize=13)
    for i in range(3):
        axs[0,i].set_xlabel('x-coordinate', fontsize=13)


    axs[1,0].set_title('Progression of distance', fontsize=13)
    axs[1,0].plot([i for i in range(nb_iter+1)], distances_all, color='magenta')
    axs[1,0].set_xlabel('Iteration', fontsize=13)
    axs[1,0].set_ylabel('Distance (km)', fontsize=13)

    axs[1,1].set_title('Last solution of each MC', fontsize=13)
    axs[1,1].plot([i for i in range(nb_iter//MClength + 1)], distances_last, color='deepskyblue')
    axs[1,1].set_xlabel('Markov Chain Number', fontsize=13)

    axs[1,2].set_title('Best solution of each MC', fontsize=13)
    axs[1,2].plot([i for i in range(nb_iter//MClength + 1)], distances_best, color='salmon')
    axs[1,2].set_xlabel('Markov Chain Number', fontsize=13)

    problem = fileName_city.split('.')
    if cooling_type == 'geometrical':
        fig.suptitle(rf'{nb_iter//MClength} Markov Chains of length {MClength} for the {problem[0]} TSP; $T_0$={temperature_begin} '+
                     rf'{cooling_type} cooling, with parameter {cooling_factor}', fontsize=16)
    elif cooling_type == 'linear':
        fig.suptitle(rf'{nb_iter//MClength} Markov Chains of length {MClength} for the {problem[0]} TSP; $T_0$={temperature_begin} '+
                     rf'{cooling_type} cooling, with parameter {decrement_param}', fontsize=16)
    
    fig.tight_layout()
    
    if save:
        name = 'Figures/compare_opt_single_sim' + str(nb_iter) + '_MC' + str(MClength) + '_T0' + str(temperature_begin) + str(cooling_type)
        plt.savefig(name + '.svg')
    if show:
        plt.show()

circuit_file = 'a280.opt.tour'
city_file = 'a280.tsp'
compare_solutions(city_file, circuit_file, temperature_begin=172, temperature_end=.001, cooling_factor=0.95, decrement_param = 0.1, 
                        nb_iter=int(3e5), MClength = 2000, cooling_type='geometrical', save=True, show=True)